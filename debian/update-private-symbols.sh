#!/bin/sh

[ -d ./debian ] || exit 1

tmpfile="$(mktemp)"
trap 'rm -- "${tmpfile}"' EXIT
while read -r line; do
    set -- $line
    file="$1"
    case $file in
    \#*)
        continue
        ;;
    *.h)
        ctags -x --c-kinds=fp "${file}" >> "${tmpfile}"
        ;;
    esac
done < debian/libwbxml2-private-dev.install
awk -v "tags=${tmpfile}" '
function parse_private_symbols(tags, symbols) {
    while ((getline < tags) > 0) {
        symbols[$1] = ""
    }
    close(tags)
}

BEGIN {
    parse_private_symbols(tags, symbols)
}

$1 ~ /@Base$/ {
    symbol = $1
    sub(/@Base$/, "", symbol)
    if (symbol in symbols) {
        printf(" %s %s 1\n", $1, $2)
    } else {
        print
    }
    next
}
{
    print
}
' debian/libwbxml2-1.symbols
